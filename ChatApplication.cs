﻿using System;
using System.Net.WebSockets;
using System.Text;
using System.Threading.Tasks;
using Newtonsoft.Json;

namespace wsTest
{
    public class ChatMessageHandler : WebSocketHandler
    {

        private struct MessageSend
        {
            public string flag;
            public string sessionId;
            public string name;
            public string message;
            public string isSelf;
            public string room;
        }

        private struct MessageReceive
        {
            public string flag;
            public string sessionId;
            public string name;
            public string message;
            public string room;
        }
        
        
        public ChatMessageHandler(WebSocketConnectionManager webSocketConnectionManager) : base(
            webSocketConnectionManager)
        {
        }

        public override async Task OnConnected(WebSocket socket)
        {
            await base.OnConnected(socket);
            var socketId = WebSocketConnectionManager.GetId(socket);
            Console.WriteLine($"{socketId} is now connected");
            var ms = new MessageSend
            {
                flag = "Self",
                sessionId = socketId,
                isSelf = "isSelf"
            };
            await SendMessageAsync(socketId,JsonConvert.SerializeObject(ms));
            ms.flag = "new ";
            ms.name = "newclient";
            await SendMessageToAllAsync(JsonConvert.SerializeObject(ms));
        }

        public override async Task ReceiveAsync(WebSocket socket, WebSocketReceiveResult result, byte[] buffer)
        {
            var socketId = WebSocketConnectionManager.GetId(socket);
            var log = $"{socketId} said: {Encoding.UTF8.GetString(buffer, 0, result.Count)}";
            Console.WriteLine(log);
            var json = Encoding.UTF8.GetString(buffer, 0, result.Count);
            try
            {
                var mr = JsonConvert.DeserializeObject<MessageReceive>(json);
                var ms = new MessageSend
                {
                    flag = "message",
                    sessionId = socketId,
                    name = mr.name,
                    room = mr.room,
                    message = mr.message,
                    isSelf = "isSelf"
                };
                await SendMessageToAllAsync(JsonConvert.SerializeObject(ms));
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
                Console.WriteLine(ex.StackTrace);
            }
        }
    }
}